#!/bin/bash

# Install Stream
echo "Install raspicar streamer"
wget http://raspbian.raspberrypi.org/raspbian/pool/main/p/python-ws4py/python3-ws4py_0.4.2+dfsg1-5_all.deb
dpkg -i python3-ws4py_0.4.2+dfsg1-5_all.deb
apt install -y python3-ws4py python3-picamera

# Install Raspicar
echo "Install raspicar backend"
apt update
apt install -y libcjson-dev libpigpio-dev build-essentials
make
make install

echo "Install raspicar frontend"
#wget 
#/var/www/html


systemctl daemon-reload
systemctl enable raspicar
systemctl enable raspicar-streamer
