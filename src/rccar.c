/*
 * Copyright (C) 2021 Thibault Cohen <thibault.cohen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "rccar.h"
#include "common.h"
#include "i2c.h"
#include "led.h"
#include "log.h"
#include "motor.h"
#include "sensorthread.h"
#include "ws.h"
#include "wsserver.h"
#include <cjson/cJSON.h>
#include <pigpio.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void carConfig(int fd, struct RCCar *car, const cJSON *data) {
  log_info("Car config - start");
  setState(car, "configuring");
  char *msg = "{\"subject\": \"config\", \"data\": {\"state\": \"start\"}}";
  wssendmessage(fd, msg);
  // LogLevel
  char *logLevel =
      cJSON_GetObjectItemCaseSensitive(data, "log_level")->valuestring;
  upperString(logLevel);
  for (int i = 0; i < 6; i++) {
    if (strcmp(logLevel, log_level_string(i)) == 0) {
      log_info("Car config - set log level to %s", logLevel);
      log_set_level(i);
    }
  }
  // Brand
  const cJSON *brand = NULL;
  brand = cJSON_GetObjectItemCaseSensitive(data, "brand");
  car->brand = brand->valuestring;
  // Model
  const cJSON *model = NULL;
  model = cJSON_GetObjectItemCaseSensitive(data, "model");
  car->model = model->valuestring;
  log_info("Car config - base done");
  // Config
  const cJSON *config = NULL;
  config = cJSON_GetObjectItemCaseSensitive(data, "config");

  // Led
  cJSON *leds_config;
  cJSON *led_config;
  leds_config = cJSON_GetObjectItemCaseSensitive(config, "leds");
  int i = 0;
  cJSON_ArrayForEach(led_config, leds_config) {
    char *ledName =
        cJSON_GetObjectItemCaseSensitive(led_config, "name")->valuestring;
    if (strcmp(ledName, "state") != 0) {
      const struct Led led = setupLed(
          ledName,
          cJSON_GetObjectItemCaseSensitive(led_config, "pin_red")->valueint,
          cJSON_GetObjectItemCaseSensitive(led_config, "pin_green")->valueint,
          cJSON_GetObjectItemCaseSensitive(led_config, "pin_blue")->valueint);
      car->leds[i] = &led;
    }
    i++;
  }
  log_info("Car config - leds done");
  // Motors
  cJSON *motors_config;
  cJSON *motor_config;
  motors_config = cJSON_GetObjectItemCaseSensitive(config, "motors");
  i = 0;
  cJSON_ArrayForEach(motor_config, motors_config) {
    const struct Motor *motor = setupMotor(
        cJSON_GetObjectItemCaseSensitive(motor_config, "name")->valuestring,
        cJSON_GetObjectItemCaseSensitive(motor_config, "pin_forward")->valueint,
        cJSON_GetObjectItemCaseSensitive(motor_config, "pin_backward")->valueint,
        cJSON_GetObjectItemCaseSensitive(motor_config, "pin_pwm")->valueint,
        cJSON_GetObjectItemCaseSensitive(motor_config, "pwm_freq")->valueint);
    car->motors[i] = motor;
    i++;
  }
  log_info("Car config - motors done");
  // Sensors
  cJSON *sensors_config;
  cJSON *sensor_config;
  sensors_config = cJSON_GetObjectItemCaseSensitive(config, "sensors");
  i = 0;
  cJSON_ArrayForEach(sensor_config, sensors_config) {
    if (strcmp(cJSON_GetObjectItemCaseSensitive(sensor_config, "proto")
                   ->valuestring,
               "i2c") == 0) {
      const struct I2cSensor *sensor = setupI2cSensor(
          cJSON_GetObjectItemCaseSensitive(sensor_config, "name")->valuestring,
          cJSON_GetObjectItemCaseSensitive(sensor_config, "model")->valuestring,
          cJSON_GetObjectItemCaseSensitive(sensor_config, "addr")->valueint);
      car->i2csensors[i] = sensor;
      i++;
    }
  }
  log_info("Car config - sensors done");

  startSensorThread(car);
  log_info("Car config - sensor thread started");

  msg = "{\"subject\": \"config\", \"data\": {\"state\": \"done\"}}";
  wssendmessage(fd, msg);
  setState(car, "configured");
  log_info("Car config - done");
}

void setState(struct RCCar *car, char *state) {
  log_info("Car set state %s", state);
  if (strcmp(state, "started") == 0) {
    gpioWrite(car->stateLed->pin_red, 1);
    gpioWrite(car->stateLed->pin_green, 0);
    gpioWrite(car->stateLed->pin_blue, 0);
  }
  if (strcmp(state, "connected") == 0) {
    gpioWrite(car->stateLed->pin_red, 1);
    gpioWrite(car->stateLed->pin_green, 1);
    gpioWrite(car->stateLed->pin_blue, 0);
  }
  if (strcmp(state, "configuring") == 0) {
    gpioWrite(car->stateLed->pin_red, 0);
    gpioWrite(car->stateLed->pin_green, 0);
    gpioWrite(car->stateLed->pin_blue, 1);
  }
  if (strcmp(state, "configured") == 0) {
    gpioWrite(car->stateLed->pin_red, 0);
    gpioWrite(car->stateLed->pin_green, 1);
    gpioWrite(car->stateLed->pin_blue, 0);
  }
}

void shutdownRCCar(struct RCCar *car) {
  log_info("Car stopping");
  stopLed(*car->stateLed);
  stopMotors(car);
  log_info("Car stopped");
}
