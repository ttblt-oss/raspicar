/*
 * Copyright (C) 2021 Thibault Cohen <thibault.cohen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "common.h"
#include "log.h"
#include "sensorthread.h"
#include "ws.h"
#include <cjson/cJSON.h>
#include <ctype.h>
#include <pigpio.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

void shutdown() {
  stopSensorThread();
  shutdownRCCar(car);
  gpioTerminate();
  // pthread_exit(NULL); // Needed ???
  exit(0);
}

void handle_sigint() {
  log_warn("Signal SIGINT captured");
  shutdown();
}

void handle_sigkill() {
  log_warn("Signal SIGKILL captured");
  shutdown();
}

void handle_sigterm() {
  log_warn("Signal SIGTERM captured");
  shutdown();
}

void handle_sigsegv() {
  log_warn("Signal SIGSEGV captured");
  shutdown();
}

void handle_sigcont() { log_warn("Signal SIGCONT captured - Do Nothing"); }

void handle_sigwinch() {
  log_warn("Signal SIGWINCH captured");
  shutdown();
}

void upperString(char *temp) {
  char *name;
  name = strtok(temp, ":");

  // Convert to upper case
  char *s = name;
  while (*s) {
    *s = toupper((unsigned char)*s);
    s++;
  }
}

long millis(){
    struct timespec _t;
    clock_gettime(CLOCK_REALTIME, &_t);
    return _t.tv_sec*1000 + lround(_t.tv_nsec/1.0e6);
}

void stopAllGpio() {
	/*
  gpioWrite(2, 0);
  gpioWrite(3, 0);
  */
  gpioWrite(4, 0);
  gpioWrite(5, 0);
  gpioWrite(6, 0);
  gpioWrite(7, 0);
  gpioWrite(8, 0);
  gpioWrite(9, 0);
  gpioWrite(10, 0);
  gpioWrite(11, 0);
  gpioWrite(12, 0);
  gpioWrite(13, 0);
  gpioWrite(14, 0);
  gpioWrite(15, 0);
  gpioWrite(16, 0);
  gpioWrite(17, 0);
  gpioWrite(18, 0);
  gpioWrite(19, 0);
  gpioWrite(20, 0);
  gpioWrite(21, 0);
  gpioWrite(22, 0);
  gpioWrite(23, 0);
  gpioWrite(24, 0);
  gpioWrite(25, 0);
  gpioWrite(26, 0);
  gpioWrite(27, 0);
}
