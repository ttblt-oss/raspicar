/*
 * Copyright (C) 2021 Thibault Cohen <thibault.cohen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "led.h"
#include "motor.h"
#include "rccar.h"
#include "ws.h"
#include <cjson/cJSON.h>
#include <pigpio.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SIGHUP 1  /* Hangup the process */
#define SIGINT 2  /* Interrupt the process */
#define SIGQUIT 3 /* Quit the process */
#define SIGILL 4  /* Illegal instruction. */
#define SIGTRAP 5 /* Trace trap. */
#define SIGABRT 6 /* Abort. */

struct RCCar *car;
int nbClientConnected;
int lastPing;

void handle_sigint();
void handle_sigkill();
void handle_sigterm();
void handle_sigcont();
void handle_sigwinch();
void handle_sigsegv();
void shutdown();
void upperString(char *temp);

long millis();
void stopAllGpio();
