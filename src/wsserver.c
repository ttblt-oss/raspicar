/*
 * Copyright (C) 2021 Thibault Cohen <thibault.cohen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "wsserver.h"
#include "common.h"
#include "led.h"
#include "log.h"
#include "motor.h"
#include "rccar.h"
#include "ws.h"
#include <cjson/cJSON.h>
#include <pigpio.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/**
 * @dir example/
 * @brief wsServer examples folder
 *
 * @file send_receive.c
 * @brief Simple send/receiver example.
 */

/**
 * @brief Called when a client connects to the server.
 *
 * @param fd File Descriptor belonging to the client. The @p fd parameter
 * is used in order to send messages and retrieve informations
 * about the client.
 */
void wsonopen(int fd) {
  char *cli;
  cli = ws_getaddress(fd);
  log_info("WS server - Connection opened, client: %d | addr: %s\n", fd, cli);
  free(cli);
  if (nbClientConnected == 0) {
    nbClientConnected++;
    setState(car, "connected");
    car->wsfd = fd;
  }
}

/**
 * @brief Called when a client disconnects to the server.
 *
 * @param fd File Descriptor belonging to the client. The @p fd parameter
 * is used in order to send messages and retrieve informations
 * about the client.
 */
void wsonclose(int fd) {
  char *cli;
  cli = ws_getaddress(fd);
  log_info("WS server - Connection closed, client: %d | addr: %s\n", fd, cli);
  free(cli);
  nbClientConnected--;
  if (nbClientConnected <= 0) {
    nbClientConnected = 0;
    setState(car, "started");
    // free(&car->wsfd);
  }
}

/**
 * @brief Called when a client connects to the server.
 *
 * @param fd File Descriptor belonging to the client. The
 * @p fd parameter is used in order to send messages and
 * retrieve informations about the client.
 *
 * @param msg Received message, this message can be a text
 * or binary message.
 *
 * @param size Message size (in bytes).
 *
 * @param type Message type.
 */
void wsonmessage(int fd, const unsigned char *str_payload, uint64_t size,
                 int type) {
  char *cli;
  cli = ws_getaddress(fd);
  log_trace("WS server - Message received %s (size: %" PRId64
            ", type: %d), from: %s/%d\n",
            str_payload, size, type, cli, fd);
  free(cli);

  const char *raw_payload = (const char *)(char *)str_payload;
  cJSON *payload = cJSON_Parse(raw_payload);
  const cJSON *raw_subject = NULL;
  raw_subject = cJSON_GetObjectItemCaseSensitive(payload, "subject");
  if (cJSON_IsString(raw_subject) && (raw_subject->valuestring != NULL)) {
    const char *subject = raw_subject->valuestring;
    const cJSON *data = NULL;
    data = cJSON_GetObjectItemCaseSensitive(payload, "data");

    if (strcmp(subject, "ping") == 0) {
      wspong(fd);
    }
    if (strcmp(subject, "setup") == 0) {
      carConfig(fd, car, data);
    }
    if (strcmp(subject, "motor") == 0) {
      setMotorSpeed(car, data);
    }
  }

  /**
   * Mimicks the same frame type received and re-send it again
   *
   * Please note that we could just use a ws_sendframe_txt()
   * or ws_sendframe_bin() here, but we're just being safe
   * and re-sending the very same frame type and content
   * again.
   */
  // ws_sendframe(fd, (char *)msg, size, true, type);
}

void wssendmessage(int fd, char *msg) {
  log_trace("WS server - send message %s\n", msg);
  ws_sendframe_txt(fd, msg, false);
}

void wspong(int fd) {
  char *msg = "{\"subject\": \"pong\"}";
  wssendmessage(fd, msg);
  lastPing = millis();
}
