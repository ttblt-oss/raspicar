/*
 * Copyright (C) 2021 Thibault Cohen <thibault.cohen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "led.h"
#include "log.h"
#include <pigpio.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct Led setupLed(char *name, int pin_red, int pin_green, int pin_blue) {
  log_info("Led creating - %s", name);
  struct Led led;
  led.name = name;
  led.pin_red = pin_red;
  led.pin_green = pin_green;
  led.pin_blue = pin_blue;
  gpioSetMode(pin_red, PI_OUTPUT);
  gpioSetMode(pin_green, PI_OUTPUT);
  gpioSetMode(pin_blue, PI_OUTPUT);
  gpioWrite(pin_red, 0);
  gpioWrite(pin_green, 0);
  gpioWrite(pin_blue, 0);
  log_info("Led created - %s", name);
  return led;
}

void setLedColor(struct Led led, int red, int green, int blue) {
  log_trace("Led set color - %s : R%d G%d B%d", led.name, red, green, blue);
  gpioWrite(led.pin_red, red);
  gpioWrite(led.pin_green, green);
  gpioWrite(led.pin_blue, blue);
}

void stopLed(struct Led led) {
  log_info("Led stopping - %s", led.name);
  setLedColor(led, 0, 0, 0);
}
