/*
 * Copyright (C) 2021 Thibault Cohen <thibault.cohen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "motor.h"
#include "common.h"
#include "log.h"
#include "rccar.h"
#include <cjson/cJSON.h>

int getPWMChannel(int pin_pwm) {
	int channel = -1;
	if (pin_pwm == 12 || pin_pwm == 18) {
		channel = 0;
	}
	if (pin_pwm == 13 || pin_pwm == 19) {
		channel = 1;
	}
	return channel;
}

struct Motor *setupMotor(char *name, int pin_forward, int pin_backward, int pin_pwm, int pwm_freq) {
  log_info("Motor creating - %s", name);
  struct Motor *motor = (struct Motor *)malloc(sizeof(struct Motor));
  motor->name = name;
 
  motor->pin_forward = pin_forward;
  gpioSetMode(motor->pin_forward, PI_OUTPUT);
  gpioWrite(motor->pin_forward, 0);

  motor->pin_backward = pin_backward;
  gpioSetMode(motor->pin_backward, PI_OUTPUT);
  gpioWrite(motor->pin_backward, 0);

  motor->pin_pwm = pin_pwm;
  motor->pwm_freq = pwm_freq;
  int channel = getPWMChannel(pin_pwm);
  if (channel == -1) {
	  log_error("Motor %s pin_pwm %s is not a PWM pin", name, pin_pwm);
	  // TODO crash
  }
  motor->pwm_channel = channel;
  gpioHardwarePWM(motor->pwm_channel, motor->pwm_freq, 0);

  log_info("Motor created - %s", name);
  return motor;
}

void setMotorSpeed(struct RCCar *car, const cJSON *data) {
  const cJSON *motor_name = NULL;
  const cJSON *speed = NULL;
  const cJSON *direction = NULL;
  motor_name = cJSON_GetObjectItemCaseSensitive(data, "motor");
  direction = cJSON_GetObjectItemCaseSensitive(data, "direction");
  speed = cJSON_GetObjectItemCaseSensitive(data, "speed");
  log_trace("Motor speed - %s %s %d", motor_name->valuestring, direction->valuestring, speed->valueint);
  for (unsigned int i = 0; i < sizeof(car->motors) / sizeof(struct Motor *);
       i++) {
    if (car->motors[i] != NULL &&
        strcmp(car->motors[i]->name, motor_name->valuestring) == 0) {

	if (strcmp(direction->valuestring, "forward") == 0){

		gpioWrite(car->motors[i]->pin_backward, 0);
		gpioWrite(car->motors[i]->pin_forward, 1);
		gpioHardwarePWM(car->motors[i]->pin_pwm, car->motors[i]->pwm_freq, speed->valueint);
	}
	if (strcmp(direction->valuestring, "backward") == 0){
		gpioWrite(car->motors[i]->pin_forward, 0);
		gpioWrite(car->motors[i]->pin_backward, 1);
		gpioHardwarePWM(car->motors[i]->pin_pwm, car->motors[i]->pwm_freq, speed->valueint);
	}
	if (strcmp(direction->valuestring, "stop") == 0){
		gpioWrite(car->motors[i]->pin_backward, 0);
		gpioWrite(car->motors[i]->pin_forward, 0);
		gpioHardwarePWM(car->motors[i]->pin_pwm, car->motors[i]->pwm_freq, 0);
	}

      return;
    }
  }
}

void stopMotors(struct RCCar *car) {
  for (unsigned int i = 0; i < sizeof(car->motors) / sizeof(struct Motor *); i++) {
    log_info("Stopping motor %s", car->motors[i]->name);
    gpioWrite(car->motors[i]->pin_backward, 0);
    gpioWrite(car->motors[i]->pin_forward, 0);
    gpioHardwarePWM(car->motors[i]->pin_pwm, car->motors[i]->pwm_freq, 0);
  }
  log_info("Motors stopped");
}
