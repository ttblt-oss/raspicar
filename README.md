

## Components

### Needed

* Raspibeerry Zero W
* [LM2596](https://www.amazon.ca/gp/product/B07YKGZD57/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)
* [Battery](https://www.amazon.ca/gp/product/B07WDF51PT/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)



# Install

## Backend

```
sudo bash install.sh
```

# Dev

## UI

```
sudo apt install npm --no-install-recommends
npm init
npm install vue
```

## Backend

```
sudo apt install libcjson-dev make libpigpio-dev clang-format clang-tidy clang build-essentials
```


# links


* https://nerdhut.de/2018/12/17/low-latency-and-high-fps-camera-stream-with-raspberry-pi/
* https://github.com/phoboslab/jsmpeg
