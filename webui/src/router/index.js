import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import TycoScorcher from "../views/tyco/Scorcher.vue";

const routes = [
  {
    path: "/tyco/scorcher",
    name: "TycoScorcher",
    component: TycoScorcher,
  },
  {
    path: "/",
    name: "Home",
    component: Home,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
