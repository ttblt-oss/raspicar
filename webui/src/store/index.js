import { createStore } from "vuex";

import * as api from "../api/index";

const state = {
  carConfig: null,
  wsClient: null,
  wsPpingStartTime: null,
  wsLatency: null,
  wsConnected: false,
  wsBattery: 0,
  wsAcc: { x: 0, y: 0, z: 0 },
  wsGyro: { x: 0, y: 0, z: 0 },
  fullscreened: false,
  camera: false,
};

const mutations = {
  connect(state, config) {
    state.wsClient = api.wsClientConnect(config, state);
  },
  setFullscreen(state, value) {
    state.fullscreened = value;
  },
  setCamera(state, value) {
    state.camera = value;
  },
};

const actions = {
  SetConfig({ state }, config) {
    state.carConfig = config;
  },
  WSConnect({ commit, state }) {
    commit("connect", state.carConfig);
  },
  WSPing({ state }) {
    if (state.wsConnected) {
      state.wsPingStartTime = new Date().getTime();
      state.wsClient.send(JSON.stringify({ subject: "ping" }));
    }
  },
  sendWSPayload({ state }, payload) {
    state.wsClient.send(JSON.stringify(payload));
  },
};

export default createStore({
  state: state,
  mutations: mutations,
  actions: actions,
  modules: {},
});
