export function wsClientConnect(config, state) {
  const wsClient = new WebSocket(config.data.wsUrl);
  wsClient.onopen = function () {
    wsClient.send(JSON.stringify(config));
    state.wsConnected = true;
  };

  wsClient.onmessage = function (evt) {
    const jsonData = JSON.parse(evt.data);
    var ping_pong_times = [];
    if (jsonData.subject == "pong") {
      const raw_latency = new Date().getTime() - state.wsPingStartTime;
      ping_pong_times.push(raw_latency);
      ping_pong_times = ping_pong_times.slice(-30); // keep last 30 samples
      var sum = 0;
      for (var i = 0; i < ping_pong_times.length; i++)
        sum += ping_pong_times[i];
      state.wsLatency = Math.round((10 * sum) / ping_pong_times.length) / 10;
      state.wsConnected = true;
    } else if (jsonData.subject == "sensor") {
      if (jsonData.data.name == "battery") {
        state.wsBattery = jsonData.data.value;
      }
      if (jsonData.data.name == "gyro") {
        state.wsAcc["x"] = jsonData.data.acc_x;
        state.wsAcc["y"] = jsonData.data.acc_y;
        state.wsAcc["z"] = jsonData.data.acc_z;
        state.wsGyro["x"] = jsonData.data.gyro_x;
        state.wsGyro["y"] = jsonData.data.gyro_y;
        state.wsGyro["z"] = jsonData.data.gyro_z;
      }
    } else {
      console.log(jsonData);
    }
  };
  wsClient.onerror = function (error) {
    state.wsConnected = false;
    console.log("error");
    console.log(error);
    state.wsLatency = null;
  };
  wsClient.onclose = function () {
    state.wsConnected = false;
    state.wsLatency = null;
  };
  return wsClient;
}
