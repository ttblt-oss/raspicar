#!/bin/bash


# Disable Some useless features
echo "Disable bluetooth"
echo "dtoverlay=disable-bt" >> /boot/config.txt
systemctl disable hciuart.service
systemctl disable bluealsa.service
systemctl disable bluetooth.service

echo "Disable Sound"
cat << EOF > /etc/modprobe.d/alsa-blacklist.conf
blacklist snd_bcm2835
EOF

# Setup RaspiConfig
echo "Setup Raspi-Config"
# raspi-config 1 System Option-> S4 change hostname
raspi-config nonint do_hostname raspicar
# raspi-config 3 Interface Option-> P1 Camera
raspi-config nonint do_camera 0
# raspi-config 3 Interface Option-> P5 I2C
raspi-config nonint do_i2c 0
# raspi-config 6 Advanced Option-> A1 Expand Filesystem
raspi-config nonint do_expand_rootfs

# Install HotSpot
echo "Install HotSpot"
apt update
apt -y install dnsmasq hostapd
systemctl stop dnsmasq
systemctl unmask hostapd
systemctl stop hostapd
systemctl stop dhcpcd
systemctl enable hostapd
systemctl enable dnsmasq
systemctl enable dhcpcd

# Configure HotSpot
echo "Configure HotSpot"
cat << EOF >/etc/dhcpcd.conf
# It is possible to fall back to a static IP if DHCP fails:
# define static profile
profile static_wlan0
  static ip_address=10.1.4.1/24
  nogateway

# fallback to static profile on wlan0
interface wlan0
  fallback static_wlan0
EOF

cat << EOF > /etc/dnsmasq.conf
interface=wlan0
dhcp-range=10.1.4.2,10.1.4.20,255.255.255.0,24h
EOF


cat << EOF > /etc/hostapd/hostapd.conf
interface=wlan0
driver=nl80211
ssid=raspicar
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=raspicar
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
EOF

car << EOF > /etc/default/hostapd
DAEMON_CONF="/etc/hostapd/hostapd.conf"
EOF

# Enable Camera streaming
echo "Enable Camera streamig"
curl https://www.linux-projects.org/listing/uv4l_repo/lpkey.asc | sudo apt-key add -
echo "deb https://www.linux-projects.org/listing/uv4l_repo/raspbian/stretch stretch main" >> /etc/apt/sources.list
apt-get install -y uv4l uv4l-raspicam uv4l-raspicam-extras uv4l-webrtc-armv6

modprobe bcm2835-v4l2 max_video_width=2592 max_video_height=1944

cat << EOF >/etc/uv4l/uv4l-raspicam.conf
driver = raspicam
auto-video_nr = yes
frame-buffers = 4
encoding = h264
width = 448
height = 360
framerate = 30
video-denoise = yes
nopreview = yes
fullscreen = no
preview = 480
preview = 240
preview = 320
preview = 240
server-option = --port=9999
server-option = --enable-webrtc=no
server-option = --enable-webrtc-audio=no
server-option = --webrtc-receive-audio=no
server-option = --webrtc-audio-layer=0
EOF
sudo systemctl stop uv4l_raspicam.service
sudo systemctl enable uv4l_raspicam.service

# Enable Nginx
echo "Install WebServer"
apt-get install -y nginx
cat << EOF > /etc/nginx/sites-available/default
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        root /var/www/html;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name _;

        location / {
              index  index.html;
              try_files $uri $uri/ /index.html;
        }
}
EOF
sudo systemctl stop nginx
sudo systemctl enable nginx

# Install Raspicar
bash update.sh


# Reboot
reboot
